/* Copyright 2008, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * curve25519-donna: Curve25519 elliptic curve, public key function
 *
 * http://code.google.com/p/curve25519-donna/
 *
 * Adam Langley <agl@imperialviolet.org>
 *
 * Derived from public domain C code by Daniel J. Bernstein <djb@cr.yp.to>
 *
 * More information about curve25519 can be found here
 *   http://cr.yp.to/ecdh.html
 *
 * djb's sample implementation of curve25519 is written in a special assembly
 * language called qhasm and uses the floating point registers.
 *
 * This is, almost, a clean room reimplementation from the curve25519 paper. It
 * uses many of the tricks described therein. Only the crecip function is taken
 * from the sample implementation.
 */

/* End of copyright notice and license */

#include <string.h>
#include <stdint.h>

typedef uint8_t u8;
typedef int32_t s32;

/* Beginning of Curve25519 coordinate-field operation functions */

typedef int64_t limb;

typedef limb felem_short[10];
typedef limb felem_long[19];

typedef felem_short felem;

/* Field element representation:
 *
 * Field elements are written as an array of signed, 64-bit limbs, least
 * significant first. The value of the field element is:
 *   x[0] + 2^26·x[1] + x^51·x[2] + 2^102·x[3] + ...
 *
 * i.e. the limbs are 26, 25, 26, 25, ... bits wide.
 */

/* Sum two numbers: output += in */
static void fsum(felem_short output, const felem_short in) {
  unsigned i;
  for (i = 0; i < 10; i += 2) {
    output[0+i] = ((s32)(output[0+i]) + (s32)(in[0+i]));
    output[1+i] = ((s32)(output[1+i]) + (s32)(in[1+i]));
  }
}

/* Find the difference of two numbers: output = in - output
 * (note the order of the arguments!)
 */
static void fdifference_backwards(felem_short output, const felem_short in) {
  unsigned i;
  for (i = 0; i < 10; ++i) {
    output[i] = ((s32)(in[i]) - (s32)(output[i]));
  }
}

/* Multiply two numbers: output = in2 * in
 *
 * output must be distinct to both inputs. The inputs are reduced coefficient
 * form, the output is not.
 */
static void fproduct(felem_long output,
                     const felem_short in2, const felem_short in) {
  output[0] =       ((limb) ((s32) in2[0])) * ((s32) in[0]);
  output[1] =       ((limb) ((s32) in2[0])) * ((s32) in[1]) +
                    ((limb) ((s32) in2[1])) * ((s32) in[0]);
  output[2] =  2 *  ((limb) ((s32) in2[1])) * ((s32) in[1]) +
                    ((limb) ((s32) in2[0])) * ((s32) in[2]) +
                    ((limb) ((s32) in2[2])) * ((s32) in[0]);
  output[3] =       ((limb) ((s32) in2[1])) * ((s32) in[2]) +
                    ((limb) ((s32) in2[2])) * ((s32) in[1]) +
                    ((limb) ((s32) in2[0])) * ((s32) in[3]) +
                    ((limb) ((s32) in2[3])) * ((s32) in[0]);
  output[4] =       ((limb) ((s32) in2[2])) * ((s32) in[2]) +
               2 * (((limb) ((s32) in2[1])) * ((s32) in[3]) +
                    ((limb) ((s32) in2[3])) * ((s32) in[1])) +
                    ((limb) ((s32) in2[0])) * ((s32) in[4]) +
                    ((limb) ((s32) in2[4])) * ((s32) in[0]);
  output[5] =       ((limb) ((s32) in2[2])) * ((s32) in[3]) +
                    ((limb) ((s32) in2[3])) * ((s32) in[2]) +
                    ((limb) ((s32) in2[1])) * ((s32) in[4]) +
                    ((limb) ((s32) in2[4])) * ((s32) in[1]) +
                    ((limb) ((s32) in2[0])) * ((s32) in[5]) +
                    ((limb) ((s32) in2[5])) * ((s32) in[0]);
  output[6] =  2 * (((limb) ((s32) in2[3])) * ((s32) in[3]) +
                    ((limb) ((s32) in2[1])) * ((s32) in[5]) +
                    ((limb) ((s32) in2[5])) * ((s32) in[1])) +
                    ((limb) ((s32) in2[2])) * ((s32) in[4]) +
                    ((limb) ((s32) in2[4])) * ((s32) in[2]) +
                    ((limb) ((s32) in2[0])) * ((s32) in[6]) +
                    ((limb) ((s32) in2[6])) * ((s32) in[0]);
  output[7] =       ((limb) ((s32) in2[3])) * ((s32) in[4]) +
                    ((limb) ((s32) in2[4])) * ((s32) in[3]) +
                    ((limb) ((s32) in2[2])) * ((s32) in[5]) +
                    ((limb) ((s32) in2[5])) * ((s32) in[2]) +
                    ((limb) ((s32) in2[1])) * ((s32) in[6]) +
                    ((limb) ((s32) in2[6])) * ((s32) in[1]) +
                    ((limb) ((s32) in2[0])) * ((s32) in[7]) +
                    ((limb) ((s32) in2[7])) * ((s32) in[0]);
  output[8] =       ((limb) ((s32) in2[4])) * ((s32) in[4]) +
               2 * (((limb) ((s32) in2[3])) * ((s32) in[5]) +
                    ((limb) ((s32) in2[5])) * ((s32) in[3]) +
                    ((limb) ((s32) in2[1])) * ((s32) in[7]) +
                    ((limb) ((s32) in2[7])) * ((s32) in[1])) +
                    ((limb) ((s32) in2[2])) * ((s32) in[6]) +
                    ((limb) ((s32) in2[6])) * ((s32) in[2]) +
                    ((limb) ((s32) in2[0])) * ((s32) in[8]) +
                    ((limb) ((s32) in2[8])) * ((s32) in[0]);
  output[9] =       ((limb) ((s32) in2[4])) * ((s32) in[5]) +
                    ((limb) ((s32) in2[5])) * ((s32) in[4]) +
                    ((limb) ((s32) in2[3])) * ((s32) in[6]) +
                    ((limb) ((s32) in2[6])) * ((s32) in[3]) +
                    ((limb) ((s32) in2[2])) * ((s32) in[7]) +
                    ((limb) ((s32) in2[7])) * ((s32) in[2]) +
                    ((limb) ((s32) in2[1])) * ((s32) in[8]) +
                    ((limb) ((s32) in2[8])) * ((s32) in[1]) +
                    ((limb) ((s32) in2[0])) * ((s32) in[9]) +
                    ((limb) ((s32) in2[9])) * ((s32) in[0]);
  output[10] = 2 * (((limb) ((s32) in2[5])) * ((s32) in[5]) +
                    ((limb) ((s32) in2[3])) * ((s32) in[7]) +
                    ((limb) ((s32) in2[7])) * ((s32) in[3]) +
                    ((limb) ((s32) in2[1])) * ((s32) in[9]) +
                    ((limb) ((s32) in2[9])) * ((s32) in[1])) +
                    ((limb) ((s32) in2[4])) * ((s32) in[6]) +
                    ((limb) ((s32) in2[6])) * ((s32) in[4]) +
                    ((limb) ((s32) in2[2])) * ((s32) in[8]) +
                    ((limb) ((s32) in2[8])) * ((s32) in[2]);
  output[11] =      ((limb) ((s32) in2[5])) * ((s32) in[6]) +
                    ((limb) ((s32) in2[6])) * ((s32) in[5]) +
                    ((limb) ((s32) in2[4])) * ((s32) in[7]) +
                    ((limb) ((s32) in2[7])) * ((s32) in[4]) +
                    ((limb) ((s32) in2[3])) * ((s32) in[8]) +
                    ((limb) ((s32) in2[8])) * ((s32) in[3]) +
                    ((limb) ((s32) in2[2])) * ((s32) in[9]) +
                    ((limb) ((s32) in2[9])) * ((s32) in[2]);
  output[12] =      ((limb) ((s32) in2[6])) * ((s32) in[6]) +
               2 * (((limb) ((s32) in2[5])) * ((s32) in[7]) +
                    ((limb) ((s32) in2[7])) * ((s32) in[5]) +
                    ((limb) ((s32) in2[3])) * ((s32) in[9]) +
                    ((limb) ((s32) in2[9])) * ((s32) in[3])) +
                    ((limb) ((s32) in2[4])) * ((s32) in[8]) +
                    ((limb) ((s32) in2[8])) * ((s32) in[4]);
  output[13] =      ((limb) ((s32) in2[6])) * ((s32) in[7]) +
                    ((limb) ((s32) in2[7])) * ((s32) in[6]) +
                    ((limb) ((s32) in2[5])) * ((s32) in[8]) +
                    ((limb) ((s32) in2[8])) * ((s32) in[5]) +
                    ((limb) ((s32) in2[4])) * ((s32) in[9]) +
                    ((limb) ((s32) in2[9])) * ((s32) in[4]);
  output[14] = 2 * (((limb) ((s32) in2[7])) * ((s32) in[7]) +
                    ((limb) ((s32) in2[5])) * ((s32) in[9]) +
                    ((limb) ((s32) in2[9])) * ((s32) in[5])) +
                    ((limb) ((s32) in2[6])) * ((s32) in[8]) +
                    ((limb) ((s32) in2[8])) * ((s32) in[6]);
  output[15] =      ((limb) ((s32) in2[7])) * ((s32) in[8]) +
                    ((limb) ((s32) in2[8])) * ((s32) in[7]) +
                    ((limb) ((s32) in2[6])) * ((s32) in[9]) +
                    ((limb) ((s32) in2[9])) * ((s32) in[6]);
  output[16] =      ((limb) ((s32) in2[8])) * ((s32) in[8]) +
               2 * (((limb) ((s32) in2[7])) * ((s32) in[9]) +
                    ((limb) ((s32) in2[9])) * ((s32) in[7]));
  output[17] =      ((limb) ((s32) in2[8])) * ((s32) in[9]) +
                    ((limb) ((s32) in2[9])) * ((s32) in[8]);
  output[18] = 2 *  ((limb) ((s32) in2[9])) * ((s32) in[9]);
}

/* Reduce a long form to a short form by taking the input mod 2^255 - 19. */
static void freduce_degree(felem_long output) {
  /* Each of these shifts and adds ends up multiplying the value by 19. */
  output[8] += output[18] << 4;
  output[8] += output[18] << 1;
  output[8] += output[18];
  output[7] += output[17] << 4;
  output[7] += output[17] << 1;
  output[7] += output[17];
  output[6] += output[16] << 4;
  output[6] += output[16] << 1;
  output[6] += output[16];
  output[5] += output[15] << 4;
  output[5] += output[15] << 1;
  output[5] += output[15];
  output[4] += output[14] << 4;
  output[4] += output[14] << 1;
  output[4] += output[14];
  output[3] += output[13] << 4;
  output[3] += output[13] << 1;
  output[3] += output[13];
  output[2] += output[12] << 4;
  output[2] += output[12] << 1;
  output[2] += output[12];
  output[1] += output[11] << 4;
  output[1] += output[11] << 1;
  output[1] += output[11];
  output[0] += output[10] << 4;
  output[0] += output[10] << 1;
  output[0] += output[10];
}

#if (-1 & 3) != 3
#error "This code only works on a two's complement system"
#endif

/* Reduce all coefficients of the short form input so that |x| <= 2^26.
 *
 * On entry: |input[i]| < 2^62
 */
static void freduce_coefficients(felem_short output, felem_long input) {
  unsigned i;

  input[10] = 0;

  for (i = 0; i < 10; i += 2) {
    limb over = input[i] >> 26;
    input[i] &= 0x3ffffff;
    input[i+1] += over;

    over = input[i+1] >> 25;
    input[i+1] &= 0x1ffffff;
    input[i+2] += over;
  }
  /* Now |input[10]| < 2 ^ 38 and all other coefficients are reduced. */
  input[0] += input[10] << 4;
  input[0] += input[10] << 1;
  input[0] += input[10];

  input[10] = 0;

  /* Now input[1..9] are reduced, and
   *    -19 * 2^38 <= input[0] < 2^26 + 19 * 2^38;
   * so |over| will be less than 77825. */
  {
    limb over = input[0] >> 26;
    input[0] &= 0x3ffffff;
    input[1] += over;
  }

  /* Now input[0,2..9] are reduced, and
   *    -77825 < input[1] < 2^25 - 1 + 77825;
   * so |over| will be no more than 1. */
  {
    /* input[1] fits in 32 bits, so we can cast before shifting here. */
    s32 over32 = (s32)(input[1]) >> 25;
    input[1] &= 0x1ffffff;
    input[2] += over32;
  }

  /* Finally, input[0,1,3..9] are reduced, and input[2] is "nearly reduced":
   * we have -1 <= input[2] <= 2^26.  This is good enough for all of our math,
   * but it will require an extra carry-propagation pass in fcontract. */

  memcpy(output, input, sizeof(felem_short));
}

/* A helpful wrapper around fproduct: output = in * in2.
 *
 * The output is reduced degree and reduced coefficient.
 */
static void
fmul(felem_short output, const felem_short in, const felem_short in2) {
  felem_long t;
  fproduct(t, in, in2);
  freduce_degree(t);
  freduce_coefficients(output, t);
}

/* Multiply a number by a scalar: output = in * scalar */
static void fscalar_product(felem_short output,
                            const felem_short in, const limb scalar) {
  unsigned i;
  felem_long tmp;
  for (i = 0; i < 10; ++i) {
    tmp[i] = in[i] * scalar;
  }
  freduce_coefficients(output, tmp);
}

static void fsquare_inner(felem_long output, const felem_short in) {
  output[0] =       ((limb) ((s32) in[0])) * ((s32) in[0]);
  output[1] =  2 *  ((limb) ((s32) in[0])) * ((s32) in[1]);
  output[2] =  2 * (((limb) ((s32) in[1])) * ((s32) in[1]) +
                    ((limb) ((s32) in[0])) * ((s32) in[2]));
  output[3] =  2 * (((limb) ((s32) in[1])) * ((s32) in[2]) +
                    ((limb) ((s32) in[0])) * ((s32) in[3]));
  output[4] =       ((limb) ((s32) in[2])) * ((s32) in[2]) +
               4 *  ((limb) ((s32) in[1])) * ((s32) in[3]) +
               2 *  ((limb) ((s32) in[0])) * ((s32) in[4]);
  output[5] =  2 * (((limb) ((s32) in[2])) * ((s32) in[3]) +
                    ((limb) ((s32) in[1])) * ((s32) in[4]) +
                    ((limb) ((s32) in[0])) * ((s32) in[5]));
  output[6] =  2 * (((limb) ((s32) in[3])) * ((s32) in[3]) +
                    ((limb) ((s32) in[2])) * ((s32) in[4]) +
                    ((limb) ((s32) in[0])) * ((s32) in[6]) +
               2 *  ((limb) ((s32) in[1])) * ((s32) in[5]));
  output[7] =  2 * (((limb) ((s32) in[3])) * ((s32) in[4]) +
                    ((limb) ((s32) in[2])) * ((s32) in[5]) +
                    ((limb) ((s32) in[1])) * ((s32) in[6]) +
                    ((limb) ((s32) in[0])) * ((s32) in[7]));
  output[8] =       ((limb) ((s32) in[4])) * ((s32) in[4]) +
               2 * (((limb) ((s32) in[2])) * ((s32) in[6]) +
                    ((limb) ((s32) in[0])) * ((s32) in[8]) +
               2 * (((limb) ((s32) in[1])) * ((s32) in[7]) +
                    ((limb) ((s32) in[3])) * ((s32) in[5])));
  output[9] =  2 * (((limb) ((s32) in[4])) * ((s32) in[5]) +
                    ((limb) ((s32) in[3])) * ((s32) in[6]) +
                    ((limb) ((s32) in[2])) * ((s32) in[7]) +
                    ((limb) ((s32) in[1])) * ((s32) in[8]) +
                    ((limb) ((s32) in[0])) * ((s32) in[9]));
  output[10] = 2 * (((limb) ((s32) in[5])) * ((s32) in[5]) +
                    ((limb) ((s32) in[4])) * ((s32) in[6]) +
                    ((limb) ((s32) in[2])) * ((s32) in[8]) +
               2 * (((limb) ((s32) in[3])) * ((s32) in[7]) +
                    ((limb) ((s32) in[1])) * ((s32) in[9])));
  output[11] = 2 * (((limb) ((s32) in[5])) * ((s32) in[6]) +
                    ((limb) ((s32) in[4])) * ((s32) in[7]) +
                    ((limb) ((s32) in[3])) * ((s32) in[8]) +
                    ((limb) ((s32) in[2])) * ((s32) in[9]));
  output[12] =      ((limb) ((s32) in[6])) * ((s32) in[6]) +
               2 * (((limb) ((s32) in[4])) * ((s32) in[8]) +
               2 * (((limb) ((s32) in[5])) * ((s32) in[7]) +
                    ((limb) ((s32) in[3])) * ((s32) in[9])));
  output[13] = 2 * (((limb) ((s32) in[6])) * ((s32) in[7]) +
                    ((limb) ((s32) in[5])) * ((s32) in[8]) +
                    ((limb) ((s32) in[4])) * ((s32) in[9]));
  output[14] = 2 * (((limb) ((s32) in[7])) * ((s32) in[7]) +
                    ((limb) ((s32) in[6])) * ((s32) in[8]) +
               2 *  ((limb) ((s32) in[5])) * ((s32) in[9]));
  output[15] = 2 * (((limb) ((s32) in[7])) * ((s32) in[8]) +
                    ((limb) ((s32) in[6])) * ((s32) in[9]));
  output[16] =      ((limb) ((s32) in[8])) * ((s32) in[8]) +
               4 *  ((limb) ((s32) in[7])) * ((s32) in[9]);
  output[17] = 2 *  ((limb) ((s32) in[8])) * ((s32) in[9]);
  output[18] = 2 *  ((limb) ((s32) in[9])) * ((s32) in[9]);
}

static void
fsquare(felem_short output, const felem_short in) {
  felem_long t;
  fsquare_inner(t, in);
  freduce_degree(t);
  freduce_coefficients(output, t);
}

/* Take a little-endian, 32-byte number and expand it into polynomial form */
static void
fexpand(felem_short output, const u8 *input) {
#define F(n,start,shift,mask) \
  output[n] = ((((limb) input[start + 0]) | \
                ((limb) input[start + 1]) << 8 | \
                ((limb) input[start + 2]) << 16 | \
                ((limb) input[start + 3]) << 24) >> shift) & mask;
  F(0, 0, 0, 0x3ffffff);
  F(1, 3, 2, 0x1ffffff);
  F(2, 6, 3, 0x3ffffff);
  F(3, 9, 5, 0x1ffffff);
  F(4, 12, 6, 0x3ffffff);
  F(5, 16, 0, 0x1ffffff);
  F(6, 19, 1, 0x3ffffff);
  F(7, 22, 3, 0x1ffffff);
  F(8, 25, 4, 0x3ffffff);
  F(9, 28, 6, 0x1ffffff);
#undef F
}

#if (-32 >> 1) != -16
#error "This code only works when >> does sign-extension on negative numbers"
#endif

/* Return 1 if input1 >= input2; return 0 otherwise.  input1 and
   input2 must have absolute value less than 2**29. */
static s32
sub_s32_geq(s32 input1, s32 input2) {
  s32 a = input1 - input2; /* a >= 0 iff input1 >= input2 */
  s32 b = a >> 31; /* b == 0 if input1 >= input2; b == -1 otherwise */
  s32 c = b + 1;

  return c;
}

/* Take a fully reduced polynomial form number and contract it into a
 * little-endian, 32-byte array
 */
static void
fcontract(u8 *output, felem_short input) {
  int i;
  felem_short tmp;

  /* freduce_coefficients left input in one of three different states:

     # All limbs are fully reduced (in the range 0..2^limbsize-1).
       In this case, we do nothing further.  (Or rather, we do a lot
       of operations that have no actual effect.)

     # input[2] == 2^26, 0 <= input[1] < 77825, and all other inputs
       are fully reduced.  In this case, we propagate a (positive)
       carry of at most 1 to higher limbs starting from input[2], and
       ending with a final carry into input[1].

     # input[2] == -1, 2^25 - 77824 <= input[1] < 2^25, and all other
       inputs are fully reduced.  In this case, we propagate a
       (negative) borrow of at most 1 from higher limbs starting from
       input[2], and ending with a final borrow from input[1].
   */

  for (i = 2; i < 9; ++i) {
    /* This calculation is a time-invariant way to propagate borrows
       and carries from input[i] into the next-larger limb.
    */
    const int shift = 26 - (i & 1);
    /* shift depends only on the loop counter, so there is no
       side-channel-resistance reason to not use an if statement
       here, but avoiding a conditional branch with
       frequently-changing condition might improve performance.
    */
    const s32 mask = 0x3ffffff >> (i & 1);
    const s32 carry = ((s32)(input[i]) >> shift);
    input[i] = (s32)(input[i]) & mask;
    input[i+1] = (s32)(input[i+1]) + carry;
  }
  {
    const s32 carry = ((s32)(input[9]) >> 25);
    input[9] = (s32)(input[9]) & 0x1ffffff;
    input[0] = (s32)(input[0]) + (carry * 19);
  }
  {
    const s32 carry = ((s32)(input[0]) >> 26);
    input[0] = (s32)(input[0]) & 0x3ffffff;
    input[1] = (s32)(input[1]) + carry;
  }

  /* Now ensure that input is not greater than 2**255 - 20. */

  /* sub_s32_geq uses considerably fewer operations than sub_s32_equal did.
   */
  s32 out_of_bounds_p
                   = sub_s32_geq(input[9], 0x1ffffff);
  out_of_bounds_p &= sub_s32_geq(input[8], 0x3ffffff);
  out_of_bounds_p &= sub_s32_geq(input[7], 0x1ffffff);
  out_of_bounds_p &= sub_s32_geq(input[6], 0x3ffffff);
  out_of_bounds_p &= sub_s32_geq(input[5], 0x1ffffff);
  out_of_bounds_p &= sub_s32_geq(input[4], 0x3ffffff);
  out_of_bounds_p &= sub_s32_geq(input[3], 0x1ffffff);
  out_of_bounds_p &= sub_s32_geq(input[2], 0x3ffffff);
  out_of_bounds_p &= sub_s32_geq(input[1], 0x1ffffff);
  out_of_bounds_p &= sub_s32_geq(input[0], 0x3ffffff - 18);

  tmp[9] = (-out_of_bounds_p) &  0x1ffffff;
  tmp[8] = (-out_of_bounds_p) &  0x3ffffff;
  tmp[7] = (-out_of_bounds_p) &  0x1ffffff;
  tmp[6] = (-out_of_bounds_p) &  0x3ffffff;
  tmp[5] = (-out_of_bounds_p) &  0x1ffffff;
  tmp[4] = (-out_of_bounds_p) &  0x3ffffff;
  tmp[3] = (-out_of_bounds_p) &  0x1ffffff;
  tmp[2] = (-out_of_bounds_p) &  0x3ffffff;
  tmp[1] = (-out_of_bounds_p) &  0x1ffffff;
  tmp[0] = (-out_of_bounds_p) & (0x3ffffff - 18);

  fdifference_backwards(tmp, input);

  tmp[1] <<= 2;
  tmp[2] <<= 3;
  tmp[3] <<= 5;
  tmp[4] <<= 6;
  tmp[6] <<= 1;
  tmp[7] <<= 3;
  tmp[8] <<= 4;
  tmp[9] <<= 6;
#define F(i, s) \
  output[s+0] |=  tmp[i] & 0xff; \
  output[s+1]  = (tmp[i] >> 8) & 0xff; \
  output[s+2]  = (tmp[i] >> 16) & 0xff; \
  output[s+3]  = (tmp[i] >> 24) & 0xff;
  output[0] = 0;
  output[16] = 0;
  F(0,0);
  F(1,3);
  F(2,6);
  F(3,9);
  F(4,12);
  F(5,16);
  F(6,19);
  F(7,22);
  F(8,25);
  F(9,28);
#undef F
}

// -----------------------------------------------------------------------------
// Shamelessly copied from djb's code
// -----------------------------------------------------------------------------
static void
crecip(felem_short out, const felem_short z) {
  felem_short z2;
  felem_short z9;
  felem_short z11;
  felem_short z2_5_0;
  felem_short z2_10_0;
  felem_short z2_20_0;
  felem_short z2_50_0;
  felem_short z2_100_0;
  felem_short t0;
  felem_short t1;
  int i;

  /* 2 */ fsquare(z2,z);
  /* 4 */ fsquare(t1,z2);
  /* 8 */ fsquare(t0,t1);
  /* 9 */ fmul(z9,t0,z);
  /* 11 */ fmul(z11,z9,z2);
  /* 22 */ fsquare(t0,z11);
  /* 2^5 - 2^0 = 31 */ fmul(z2_5_0,t0,z9);

  /* 2^6 - 2^1 */ fsquare(t0,z2_5_0);
  /* 2^7 - 2^2 */ fsquare(t1,t0);
  /* 2^8 - 2^3 */ fsquare(t0,t1);
  /* 2^9 - 2^4 */ fsquare(t1,t0);
  /* 2^10 - 2^5 */ fsquare(t0,t1);
  /* 2^10 - 2^0 */ fmul(z2_10_0,t0,z2_5_0);

  /* 2^11 - 2^1 */ fsquare(t0,z2_10_0);
  /* 2^12 - 2^2 */ fsquare(t1,t0);
  /* 2^20 - 2^10 */ for (i = 2;i < 10;i += 2) { fsquare(t0,t1); fsquare(t1,t0); }
  /* 2^20 - 2^0 */ fmul(z2_20_0,t1,z2_10_0);

  /* 2^21 - 2^1 */ fsquare(t0,z2_20_0);
  /* 2^22 - 2^2 */ fsquare(t1,t0);
  /* 2^40 - 2^20 */ for (i = 2;i < 20;i += 2) { fsquare(t0,t1); fsquare(t1,t0); }
  /* 2^40 - 2^0 */ fmul(t0,t1,z2_20_0);

  /* 2^41 - 2^1 */ fsquare(t1,t0);
  /* 2^42 - 2^2 */ fsquare(t0,t1);
  /* 2^50 - 2^10 */ for (i = 2;i < 10;i += 2) { fsquare(t1,t0); fsquare(t0,t1); }
  /* 2^50 - 2^0 */ fmul(z2_50_0,t0,z2_10_0);

  /* 2^51 - 2^1 */ fsquare(t0,z2_50_0);
  /* 2^52 - 2^2 */ fsquare(t1,t0);
  /* 2^100 - 2^50 */ for (i = 2;i < 50;i += 2) { fsquare(t0,t1); fsquare(t1,t0); }
  /* 2^100 - 2^0 */ fmul(z2_100_0,t1,z2_50_0);

  /* 2^101 - 2^1 */ fsquare(t1,z2_100_0);
  /* 2^102 - 2^2 */ fsquare(t0,t1);
  /* 2^200 - 2^100 */ for (i = 2;i < 100;i += 2) { fsquare(t1,t0); fsquare(t0,t1); }
  /* 2^200 - 2^0 */ fmul(t1,t0,z2_100_0);

  /* 2^201 - 2^1 */ fsquare(t0,t1);
  /* 2^202 - 2^2 */ fsquare(t1,t0);
  /* 2^250 - 2^50 */ for (i = 2;i < 50;i += 2) { fsquare(t0,t1); fsquare(t1,t0); }
  /* 2^250 - 2^0 */ fmul(t0,t1,z2_50_0);

  /* 2^251 - 2^1 */ fsquare(t1,t0);
  /* 2^252 - 2^2 */ fsquare(t0,t1);
  /* 2^253 - 2^3 */ fsquare(t1,t0);
  /* 2^254 - 2^4 */ fsquare(t0,t1);
  /* 2^255 - 2^5 */ fsquare(t1,t0);
  /* 2^255 - 21 */ fmul(out,t1,z11);
}

/* End of Curve25519 coordinate-field operation functions */

/* Input: Q, Q', Q-Q'
 * Output: 2Q, Q+Q'
 *
 *   x2 z2: short form
 *   x3 z3: short form
 *   x z: short form, destroyed
 *   xprime zprime: short form, destroyed
 *   qmqp: short form, preserved
 */
static void fmonty(felem_short x2, felem_short z2,  /* output 2Q */
                   felem_short x3, felem_short z3,  /* output Q + Q' */
                   felem_short x, felem_short z,    /* input Q */
                   felem_short xprime, felem_short zprime,  /* input Q' */
                   const felem_short qmqp /* input Q - Q' */) {
  felem_short origx, origxprime,
    zzz, xx, zz, xxprime, zzprime, zzzprime, xxxprime;

  memcpy(origx, x, sizeof(felem_short));
  fsum(x, z);
  fdifference_backwards(z, origx);  // does x - z

  memcpy(origxprime, xprime, sizeof(felem_short));
  fsum(xprime, zprime);
  fdifference_backwards(zprime, origxprime);
  fmul(xxprime, xprime, z);
  fmul(zzprime, x, zprime);
  memcpy(origxprime, xxprime, sizeof(felem_short));
  fsum(xxprime, zzprime);
  fdifference_backwards(zzprime, origxprime);
  fsquare(xxxprime, xxprime);
  fsquare(zzzprime, zzprime);
  fmul(zzprime, zzzprime, qmqp);
  memcpy(x3, xxxprime, sizeof(felem_short));
  memcpy(z3, zzprime, sizeof(felem_short));

  fsquare(xx, x);
  fsquare(zz, z);
  fmul(x2, xx, zz);
  fdifference_backwards(zz, xx);  // does zz = xx - zz
  fscalar_product(zzz, zz, 121665);
  fsum(zzz, xx);
  fmul(z2, zz, zzz);
}

/* Conditionally swap two reduced-form limb arrays if 'iswap' is 1, but leave
 * them unchanged if 'iswap' is 0.  Runs in data-invariant time to avoid
 * side-channel attacks.
 *
 * NOTE that this function requires that 'iswap' be 1 or 0; other values give
 * wrong results.  Also, the two limb arrays must be in reduced-coefficient,
 * reduced-degree form: the values in a[10..19] or b[10..19] aren't swapped,
 * and all all values in a[0..9],b[0..9] must have magnitude less than
 * INT32_MAX.
 */
static void
swap_conditional(felem_short a, felem_short b, limb iswap) {
  unsigned i;
  const s32 swap = -iswap;

  for (i = 0; i < 10; ++i) {
    const s32 x = swap & ( ((s32)a[i]) ^ ((s32)b[i]) );
    a[i] = ((s32)a[i]) ^ x;
    b[i] = ((s32)b[i]) ^ x;
  }
}

/* Calculates nQ where Q is the x-coordinate of a point on the curve
 *
 *   resultx/resultz: the x coordinate of the resulting curve point (short form)
 *   n: a little endian, 32-byte number
 *   q: a point of the curve (short form)
 */
static void
cmult(felem_short resultx, felem_short resultz,
      const u8 *n, const felem_short q) {
  felem_short a = {0}, b = {1}, c = {1}, d = {0};
  felem_short *nqpqx = &a, *nqpqz = &b, *nqx = &c, *nqz = &d, *t;
  felem_short e = {0}, f = {1}, g = {0}, h = {1};
  felem_short *nqpqx2 = &e, *nqpqz2 = &f, *nqx2 = &g, *nqz2 = &h;

  unsigned i, j;

  memcpy(*nqpqx, q, sizeof(felem_short));

  for (i = 0; i < 32; ++i) {
    u8 byte = n[31 - i];
    for (j = 0; j < 8; ++j) {
      const limb bit = byte >> 7;

      swap_conditional(*nqx, *nqpqx, bit);
      swap_conditional(*nqz, *nqpqz, bit);
      fmonty(*nqx2, *nqz2,
             *nqpqx2, *nqpqz2,
             *nqx, *nqz,
             *nqpqx, *nqpqz,
             q);
      swap_conditional(*nqx2, *nqpqx2, bit);
      swap_conditional(*nqz2, *nqpqz2, bit);

      t = nqx;
      nqx = nqx2;
      nqx2 = t;
      t = nqz;
      nqz = nqz2;
      nqz2 = t;
      t = nqpqx;
      nqpqx = nqpqx2;
      nqpqx2 = t;
      t = nqpqz;
      nqpqz = nqpqz2;
      nqpqz2 = t;

      byte <<= 1;
    }
  }

  memcpy(resultx, *nqx, sizeof(felem_short));
  memcpy(resultz, *nqz, sizeof(felem_short));
}

int
curve25519_donna(u8 *mypublic, const u8 *secret, const u8 *basepoint) {
  felem_short bp, x, z, zmone;
  uint8_t e[32];
  int i;

  for (i = 0; i < 32; ++i) e[i] = secret[i];
  e[0] &= 248;
  e[31] &= 127;
  e[31] |= 64;

  fexpand(bp, basepoint);
  cmult(x, z, e, bp);
  crecip(zmone, z);
  fmul(z, x, zmone);
  fcontract(mypublic, z);
  return 0;
}
